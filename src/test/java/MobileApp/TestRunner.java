package MobileApp;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

@RunWith(value = Cucumber.class)
@CucumberOptions(
        plugin = {"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"},
        monochrome = true,
        features = {"src/test/resources/Features"},
        tags = {"@performance"},
        glue = {"MobileApp.Steps"})

public class TestRunner 
{
    @AfterClass
    public static void writeExtentReport() {
    }
}
