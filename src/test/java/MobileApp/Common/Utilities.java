package MobileApp.Common;

import MobileApp.Base.BaseDriver;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;
import java.util.HashMap;
import java.util.List;

public class Utilities extends BaseDriver
{
    public MobileElement WaitForElement(AppiumDriver driver, MobileElement mobileElement)
    {
        try{
            WebDriverWait wait = new WebDriverWait(driver, 10);
            wait.until(ExpectedConditions.elementToBeClickable(mobileElement));
            return mobileElement;
        }catch(Exception e)
        {
            throw e;
        }
    }

    public void scrollDown() {

        int height = Driver.manage().window().getSize().getHeight();

        PointOption bottom = PointOption.point(5, height * 2 / 3);
        PointOption top = PointOption.point(5, height / 3);

        new TouchAction(Driver).press(bottom)
                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(2000)))
                .moveTo(top)
                .release().perform();

    }

    /* We can gather other performance metrics by passing the respective instruments value like cpuInfo, batteryInfo */

    private HashMap<String, Integer> getPerformanceMetrics(AndroidDriver driver) {
        List<List<Object>> data = driver.getPerformanceData("com.lalamove.techchallenge", "memoryinfo", 10);
        HashMap<String, Integer> readableData = new HashMap<>();
        for (int i = 0; i < data.get(0).size(); i++) {
            int val;
            if (data.get(1).get(i) == null) {
                val = 0;
            } else {
                val = Integer.parseInt((String) data.get(1).get(i));
            }
            readableData.put((String) data.get(0).get(i), val);
        }
        return readableData;
    }

    public void getAppPerformanceInfo() {

        HashMap<String, Integer> memoryInfo = getPerformanceMetrics((AndroidDriver) Driver);

        System.out.println(memoryInfo);

    }
}
