package MobileApp.Base;

import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class BasePage extends BaseDriver
{
    protected BasePage()
    {
        PageFactory.initElements(new AppiumFieldDecorator(Driver),this);
    }
}
