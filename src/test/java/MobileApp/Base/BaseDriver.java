package MobileApp.Base;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static io.appium.java_client.remote.AndroidMobileCapabilityType.APP_ACTIVITY;
import static io.appium.java_client.remote.AndroidMobileCapabilityType.APP_PACKAGE;
import static io.appium.java_client.remote.MobileCapabilityType.*;

public class BaseDriver {

    protected BaseDriver()
    {
        if (Driver == null)
        {
            GetDefaultDriver();
        }
    }

    protected AppiumDriver<MobileElement> Driver;

    private AppiumDriver<MobileElement> GetDefaultDriver() {

        URL url = null;
        try {
            url = new URL("http://127.0.0.1:4723/wd/hub");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }


        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(AUTOMATION_NAME, "UiAutomator2");
        capabilities.setCapability("uiautomator2ServerLaunchTimeout", 60000);
        capabilities.setCapability(PLATFORM_NAME, "Android");
        capabilities.setCapability(DEVICE_NAME, "05ff060f004d107d");
        capabilities.setCapability(APP_PACKAGE, "com.lalamove.techchallenge");
        capabilities.setCapability(APP, System.getProperty("user.dir") + "/app/app_Android.apk");
        capabilities.setCapability(APP_ACTIVITY, "com.lalamove.techchallenge.app.MainActivity");
        capabilities.setCapability("noResetValue","false");
        Driver = new AndroidDriver<>(url, capabilities);
        Driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        return Driver;
    }
}
