package MobileApp.Pages;

import MobileApp.Base.BasePage;
import MobileApp.Common.Utilities;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

import java.util.List;

public class DeliveryListsPage extends BasePage {

    private Utilities utilities = new Utilities();

    @AndroidFindBy(className = "androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup")
    private List<MobileElement> deliveryList;

    @AndroidFindBy(xpath = "//*[@resource-id='com.lalamove.techchallenge:id/toolbar']")
    private MobileElement ListHeader;

    @AndroidFindBy(xpath = "//*[@resource-id='com.lalamove.techchallenge:id/textView_description']")
    private List<MobileElement> deliveryTextDescription;

    @AndroidFindBy(xpath = "//*[@resource-id='com.lalamove.techchallenge:id/textView_address']")
    private List<MobileElement> deliveryAddress;

    public void ViewAllDeliveryTasks() {
        utilities.WaitForElement(Driver, ListHeader).isDisplayed();
    }

    public void PerformScroll()
    {
        utilities.scrollDown();
    }

    /* Returns the first available task details */

    public String GetAvailableDeliveryTaskName() {

        String[] allText = new String[deliveryTextDescription.size()];

        int i = 0;

        for(MobileElement element : deliveryTextDescription)
        {
            allText[i++] = element.getText();
        }

        return allText[0];
    }

    public String GetAvailableDeliveryTaskAddress() {

        String[] allText = new String[deliveryAddress.size()];

        int j = 0;

        for(MobileElement element : deliveryAddress)
        {
            allText[j++] = element.getText();
        }

        return allText[0];
    }

    public void ClickOnTheDeliveryTask(String deliveryTaskName) {
        deliveryTextDescription.stream().filter(i -> i.getText().contains(deliveryTaskName)).findFirst().get().click();
    }
}
