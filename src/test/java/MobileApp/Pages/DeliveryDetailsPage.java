package MobileApp.Pages;

import MobileApp.Base.BasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class DeliveryDetailsPage extends BasePage {

    @AndroidFindBy(xpath = "//*[@resource-id='com.lalamove.techchallenge:id/tvDescription']")
    private MobileElement deliveryText;

    @AndroidFindBy(xpath = "//*[@resource-id='com.lalamove.techchallenge:id/textView_address']")
    private MobileElement deliveryAddress;

    public String GetSelectedTaskName() {
        return deliveryText.getText();
    }

    public String GetSelectedTaskAddress() {
        return deliveryAddress.getText();
    }
}
