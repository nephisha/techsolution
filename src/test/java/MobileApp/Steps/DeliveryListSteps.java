package MobileApp.Steps;

import MobileApp.Common.Utilities;
import MobileApp.Pages.DeliveryDetailsPage;
import MobileApp.Pages.DeliveryListsPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class DeliveryListSteps {

    private DeliveryListsPage deliveryListsPage = new DeliveryListsPage();
    private DeliveryDetailsPage deliveryDetailsPage = new DeliveryDetailsPage();
    private Utilities utilities = new Utilities();

    private String deliveryTaskName = "";
    private String deliveryTaskAddress = "";

    @Given("Launch the app to see the delivery tasks list")
    public void launchTheAppToSeeTheDeliveryTasksList() {
        deliveryListsPage.ViewAllDeliveryTasks();
        deliveryListsPage.PerformScroll();
    }

    @When("I perform a scroll")
    public void iPerformAScroll() {
        deliveryListsPage.PerformScroll();
    }

    @Then("I should see the added delivery tasks")
    public void iShouldSeeTheAddedDeliveryTasks() {

    }

    @When("I tap on the delivery task")
    public void iTapOnTheDeliveryTask() {
        deliveryTaskName = deliveryListsPage.GetAvailableDeliveryTaskName();
        deliveryTaskAddress = deliveryListsPage.GetAvailableDeliveryTaskAddress();
        deliveryListsPage.ClickOnTheDeliveryTask(deliveryTaskName);
    }

    @Then("I should see its delivery details")
    public void iShouldSeeItsDeliveryDetails() {
        Assert.assertEquals(deliveryTaskName, deliveryDetailsPage.GetSelectedTaskName());
        Assert.assertEquals(deliveryTaskAddress, deliveryDetailsPage.GetSelectedTaskAddress());
    }

    @And("I {string} profiling the app for performance data")
    public void iProfilingTheAppForPerformanceData(String timer) {
        utilities.getAppPerformanceInfo();
    }

    @When("I long press on the delivery task")
    public void iLongPressOnTheDeliveryTask() {
        /* Didn't implement as the app was crashing */
    }

    @Then("I should see the delivery task removed")
    public void iShouldSeeTheDeliveryTaskRemoved() {
        /* Didn't implement as the app was crashing */
    }
}
