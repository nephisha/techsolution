Feature: Delivery Lists
  As a User
  I should be listed with all available delivery tasks
  So that I can view their delivery details

  @scroll
  Scenario: Verify that all delivery tasks are listed
    Given Launch the app to see the delivery tasks list
    When I perform a scroll
    Then I should see the added delivery tasks

  @test01
  Scenario: Verify the description and address is displayed correctly for the selected task
    Given Launch the app to see the delivery tasks list
    When I tap on the delivery task
    Then I should see its delivery details

  @performance
  Scenario: Profile the app for performance
    Given Launch the app to see the delivery tasks list
    And I "start" profiling the app for performance data
    When I tap on the delivery task
    Then I should see its delivery details
    And I "stop" profiling the app for performance data

#    App is crashing when performing the long press operation
#  @test02
#  Scenario: Verify whether the task is removed when long pressed on it
#    Given Launch the app to see the delivery tasks list
#    When I long press on the delivery task
#    Then I should see the delivery task removed

