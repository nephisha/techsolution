## Cucumber Appium - BDD Framework

**Cucumber JAVA Framework to automate mobile app tests using Appium Driver**

Test Framework was designed in JAVA following BDD style to propose a common way how people should create Acceptance tests.

**Pre-requisites:**

1. Have the Android SDK installed.
2. Have the Appium desktop version installed, I have used v1.13
3. Have the maven installed and the set the $PATH variables 
4. IntelliJ IDE and have the cucumber plugin installed
5. Required dependencies are added in the POM.xml file

How to run the tests?

1. Modify the DEVICE_NAME in the Capablity with the Device ID of real phone, where the test is executed.
2. Tests can be executed using the cucumberRunner.class - By just running the  TestRunner.java class 
3. Or can be executed in cmd line using maven - `mvn clean test -Dcucumber.options="--tags @test"`

**Reporting**

I have used Extent Reports for creating the test report for each run.

You can see the reports in the folder "test-output"

![image](https://user-images.githubusercontent.com/6779295/60098617-4c746e00-9799-11e9-83a3-63ce099caf35.png)

**Folder Structure:**

![image](https://user-images.githubusercontent.com/6779295/60096566-a292e280-9794-11e9-8708-289f1306e9ad.png)

- The apk file is placed in the app folder.
- Gherkin styled acceptance tests are written under the /resources/Features folder
- Extent Reports .properties and config.xml are placed under the resources folder.


